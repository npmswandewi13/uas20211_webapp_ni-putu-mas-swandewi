from django.db import models
from accounts import models as account_models
from accounts.models import CustomUser
import datetime

# Create your models here.
class ConnectStrava(models.Model):
    SPORT = (
        ("Ride", "Ride"),
        ('Run', 'Run'), 
        ('Swim', 'Swim'),
        ('Hike', 'Hike'),
        ('Walk', 'Walk'),
        ('Alpine Ski', 'Alpine Ski'),
        ('Yoga', 'Yoga'),
        ('Workout', 'Workout'),
        ('Virtual Run', 'Virtual Run'),
        ('Virtual Ride', 'Virtual Ride'),
    )

class MyActivity(models.Model):
    email = models.ForeignKey(account_models.CustomUser, on_delete=models.CASCADE)
    sport = models.ForeignKey(ConnectStrava, on_delete=models.CASCADE, default='Virtual Run')
    date_time = models.DateTimeField(default=datetime.datetime.now)
    title = models.CharField()
    distance = models.IntegerField()
    elevation = models.IntegerField()
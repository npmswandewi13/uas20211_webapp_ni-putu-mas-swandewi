from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import fields

class CustomCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2']
    
    first_name = forms.Field(widget=forms.TextInput(attrs={
        'class': 'form-label-wrapper form-input', 'placeholder': 'Enter your first name'
    }), label='First Name')
    last_name = forms.Field(widget=forms.TextInput(attrs={
        'class': 'form-label-wrapper form-input', 'placeholder': 'Enter your last name'
    }), label='Last Name')
    email = forms.Field(widget=forms.EmailInput(attrs={
        'class': 'form-label-wrapper form-input', 'placeholder': 'Enter your email'
    }))
    password1 = forms.Field(widget=forms.PasswordInput(attrs={
        'class': 'form-label-wrapper form-input', 'placeholder': 'Enter your password'
    }), label='Password')
    password2 = forms.Field(widget=forms.PasswordInput(attrs={
        'class': 'form-label-wrapper form-input', 'placeholder': 'Enter your password again'
    }), label='Confirm Password')

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.username = instance.email
        if commit:
            instance.save()
        return instance
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone

# Create your models here.
class CustomUser(AbstractBaseUser, PermissionsMixin):
    created_at = models.DateTimeField(default=timezone.now)
    fullname = models.CharField(max_length=255, default='')
    email = models.EmailField(max_length=255, unique=True)
    phone = models.CharField(max_length=20, default='')
    photo = models.FileField(upload_to='user/photo/', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False) # <--- yang bisa akses django admin
    is_superuser = models.BooleanField(default=False) # <--- yang bisa akses django admin

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullname', 'phone']


    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        return super().save(*args, **kwargs)

    def __str__(self):
        return '{} / {}'.format(self.email, self.fullname)
